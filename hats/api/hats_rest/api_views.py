from django.shortcuts import render
from .models import LocationVO, Hat
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from django.http import JsonResponse
import json


# Create your views here.
class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name",
        "section_number",
        "shelf_number",
    ]
class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "fabric",
        "style_name",
        "color",
        # "picture_url",
        # "location",
    ]
    # encoders= {"location": LocationVODetailEncoder()}

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
    ]
    encoders= {"location": LocationVODetailEncoder()}



@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse({"hats": hats}, encoder=HatListEncoder)
    else:
        content = json.loads(request.body)
        try:
            location_href= content["location"]
            # location_closetname = f"/api/locations/{location_vo_id}/"

            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

    hats = Hat.objects.create(**content)
    return JsonResponse(
        hats,
        encoder=HatDetailEncoder,
        safe=False,
    )

@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_hats(request, pk):
    if request.method == "GET":
        hats = Hat.objects.get(id=pk)
        return JsonResponse(hats, encoder=HatDetailEncoder, safe=False)
    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "location" in content:
                locationVO = LocationVO.objects.get(import_href=content["location"])
                content["location"] = locationVO
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Location"},
                status=400,
            )
        Hat.objects.filter(id=pk).update(**content)
        hats = Hat.objects.get(id=pk)
        return JsonResponse(
            hats,
            encoder=HatDetailEncoder,
            safe=False,
        )
