from django.db import models

class LocationVO(models.Model):
    import_href = models.CharField(max_length=50, unique=True, null=True)
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()

class Hat(models.Model):
    fabric = models.CharField(max_length=60)
    style_name = models.CharField(max_length=50)
    color = models.CharField(max_length=50)
    picture_url = models.URLField(null=True, blank=True)
    location = models.ForeignKey(LocationVO, related_name= "hats", on_delete=models.CASCADE, blank=True, null=True )
