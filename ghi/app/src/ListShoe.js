import React from 'react';
import { Link } from 'react-router-dom';


class ShoeList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        shoes: []
    };
  }

  async remove(id){
    await fetch(`http://localhost:8080/api/shoes/${id}`,{
        method: 'DELETE',
        headers: {
            'Accept' : 'application/json',
            'Content-Type' : 'application/json'
        }
    })
    let updatedShoes = this.state.shoes.filter(i => i.id !== id);
    this.setState({shoes : updatedShoes});
}


  async componentDidMount() {
    const url = 'http://localhost:8080/api/shoes/';

    try {
      const response = await fetch(url);
      if (response.ok) {
        // Get the list of conferences
        const data = await response.json();

        // Create a list of for all the requests and
        // add all of the requests to it
        console.log("<---data.shoes--->", data.shoes)
        await this.setState({shoes: data.shoes})
        // need the wait ^
      }
    } catch(e){
        window.alert(e)
      }
    }


  render() {
    return (
      <>
        <div >
          <img className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" alt="" width="600" />
          <h1 className="display-5 fw-bold">Shoe List!</h1>
          <div class="col-md-12  text-end">
            <button className="btn btn-warning" ><Link to="/create-shoes"> Create List </Link></button>
          </div>
          <div className="col-lg-6 mx-auto">
          <br></br>
            <p className="display-5">My Shoes</p>
            <br></br>
          </div>
        </div>
        <div className="container">
          <h2>Upcoming conferences</h2>
          <div className="row">
            {this.state.shoes.map((shoe) => {
              return (
                <div className="card mb-3 shadow">
                <div className="card-body">
                <h3>{shoe.model_name}</h3>
                <h5>{shoe.manufacturer}</h5>
                <img src={shoe.picture_url} width="200" height="150"></img>
                <footer>
                    <button className="btn btn-warning" onClick={() => this.remove(shoe.id)}><Link to="/list"> Delete </Link></button>
                </footer>
                </div>
                </div>
              );
            })}
          </div>
        </div>
      </>
    );
  }
}

export default ShoeList;
