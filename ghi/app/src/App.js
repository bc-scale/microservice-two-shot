import { BrowserRouter, Routes, Route } from 'react-router-dom';
import HatList from './HatList';
import HatForm from './HatForm';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeList from './ListShoe';
import ShoeForm from './CreateShoe';

function App() {

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats" element={<HatList />}/>
          <Route path="/hats/new" element={<HatForm />} />
          <Route path="/create-shoes" element={<ShoeForm />}/>
          <Route path="/list" element={<ShoeList />}/>

        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
