import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()

# Import models from hats_rest, here.
# from shoes_rest.models import Something

from shoes_rest.models import BinVO

def get_bins():
    url = "http://wardrobe-api:8000/api/bins/"
    reponse = requests.get(url)
    content = json.loads(reponse.content)
    print(content)
    for bin in content["bins"]:
        BinVO.objects.update_or_create(
            import_href=bin["href"],
            defaults={
                "closet_name": bin["closet_name"]}           
        )



def poll():
    while True:
        try:
            # Write your polling logic, here
            print('Shoes poller polling nuts data')
            get_bins()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)



if __name__ == "__main__":
    poll()

import json
import requests
